import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';

void main() {
  test('{{name}}Model is a subtype of {{name}}Entity', () async {
    final _t{{name.pascalCase()}}Model = {{name.pascalCase()}}Model.dummy();
    expect(_t{{name.pascalCase()}}Model, isA<{{name.pascalCase()}}Entity>());
  });

  group('fromJson', () {
    final _t{{name.pascalCase()}}Model = {{name.pascalCase()}}Model.dummy();
    test('should return a valid model', () {
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('copy path json'))['data']
              ['{{nameStringPath}}'];
      final result = {{name.pascalCase()}}Model.fromJson(jsonMap);
      expect(result, _t{{name.pascalCase()}}Model);
    });
  });
}