import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockNetworkInfo extends Mock implements NetworkInfo{}
class Mock{{name.pascalCase()}}RemoteDataSource extends Mock implements {{name.pascalCase()}}RemoteDataSource{}
void main() {
  late MockNetworkInfo mockNetworkInfo;
  late Mock{{name.pascalCase()}}RemoteDataSource = mock{{name.pascalCase()}}RemoteDataSource;
  late {{name.pascalCase()}}RepositoryImpl = {{name.camelCase()}}RepositoryImpl;

  setUp((){
      mockNetworkInfo = MockNetworkInfo();
      mock{{name.pascalCase()}}RemoteDataSource = Mock{{name.pascalCase()}}RemoteDataSource;
      {{name.camelCase()}}RepositoryImpl = {{name.pascalCase()}}RepositoryImpl(networkInfo:mockNetworkInfo,{{name.camelCase()}}RemoteDataSource: mock{{name.pascalCase()}}RemoteDataSource);
    });
    final _t{{nameModel.pascalCase()}}Data = {{nameModel.pascalCase()}}Model.dummy();
    group('Enter name group',(){
        group('When device is online',(){
          setUp((){
            when(()=> mockNetworkInfo.isConnected).thenAnswer((_) async => true);
          });
          group('right side >',(){
            test('should return the right side of [{{nameModel.pascalCase()}}Entities]',() async {
              when(() => mock{{name.pascalCase()}}RemoteDataSource.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy()),).thenAnswer((_) async => _t{{nameModel.pascalCase()}}Data);
              final remoteData = await mock{{name.pascalCase()}}RemoteDataSource.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
              final result = await {{name.camelCase()}}RepositoryImpl.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
              expect(result,Right(remoteData));
              verify(() => mock{{name.pascalCase()}}RemoteDataSource.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).called(2);
              verifyNoMoreInteractions(mock{{name.pascalCase()}}RemoteDataSource);
            });
          });
        group('left side >',(){
        test('should return the left side of [Failure]',() async {
          when(() => mock{{name.pascalCase()}}RemoteDataSource.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).thenThrow(Failure.dummy());
          late Object result;
          try{
            final result = await {{name.camelCase()}}RepositoryImpl.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
            return Right(result);
          }catch (e){
            result = Left(GraphQlExceptionHandler.exceptionToFailure(exception:e));
          }
          expect(result,const Left(ServerFailure(message:"Message"));
          verify(() => mock{{name.pascalCase()}}RemoteDataSource.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).called(1);
          verifyNoMoreInteractions(mock{{name.pascalCase()}}RemoteDataSource);
        });
      });
    });
    group('When device is offline',(){
      setUp((){
        when(()=> mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      group('left side >',(){
        test('should return the left side of [Failure]',() async {
          final result = await {{name.camelCase()}}RepositoryImpl.{{methodDataSource.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
          expect(result,const Left(ServerFailure(message:'No Internet')));
        });
      });
    });
  });
}  