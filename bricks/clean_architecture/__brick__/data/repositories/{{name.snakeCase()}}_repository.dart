import 'package:dartz/dartz.dart';

class {{name}}RepositoryImpl implement {{name}}Repository{
  {{name}}RemoteDataSource {{name.lowerCase()}}RemoteDataSource;
  final NetworkInfo networkInfo;

  {{name}}RepositoryImpl({required this.{{name.lowerCase()}}RemoteDataSource, required this.networkInfo});

   /* Example:
   
  @override
  Future<Either<Failure,DefaultEntity>> default({required Param param}) async {
    if(await networkInfo.isConnected){
      try{
        final result = await {{name.lowerCase()}}RemoteDataSource.default(param:param);
        return Right(result);
      }on AppException catch(e){
        final Failure failure = e.toFailure();
        return Left(failure);
      }on SocketException catch(e){
        return Left(SocketFailure(message:e.message));
      }
    } else {
      return const Left(ServerFailure(message:'No Internet'));
    }
  }

  */
}