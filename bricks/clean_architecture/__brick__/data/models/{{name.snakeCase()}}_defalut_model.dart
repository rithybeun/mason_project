/*Example:

  class DefaultModel extends DefaultEntity{
   
    const DefaultModel({required String default}):super(default:default);
  }
  
  factory DefaultModel.fromJson(Map<String,dynamic> json){
    return DefaultMode(default: json['default']);
  }

  factory DefaultModel.dummy(){
    return const DefaultMode(default:'default');
  }

*/