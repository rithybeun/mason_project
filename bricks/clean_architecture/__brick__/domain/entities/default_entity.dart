import 'package:equatable/equatable.dart';

/*Example:

  class DefaultEntity extends Equatable{
    final String default;
    const DefaultEntity({required this.default});
  }
  @override
  List<Object?> get props => [default];

*/