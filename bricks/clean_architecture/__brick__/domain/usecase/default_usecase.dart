import 'package:dartz/dartz.dart';

/*Example:

  class DefaultUseCase implements UseCase<DefaultEntity,Param>{
    final {{name}}Repostiry repository;
    const DefaultUseCase({required this.repository});

    @override
    Future<Either<Failurem,DefaultEntity>> call({required Param param}) async{
      return await repository.default(param:param);
    }
  }


*/