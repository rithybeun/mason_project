import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';



class MockJarvis extends Mock implements Javis{}

void main(){
  TestWidgetsFlutterBinding.ensureInitialized();
  late MockJarvis mockJarvis;
  late {{name.pascalCase()}}RemoteDataSourceImpl {{name.camelCase()}}RemoteDataSourceImpl;

  setUp((){
    mockJarvis = MockJarvis();
    {{name.camelCase()}}RemoteDataSourceImpl = {{name.pascalCase()}}RemoteDataSourceImpl();
  });
  final QueryDocument queryDocument = QueryDocument();
  final _t{{nameModel.pascalCase()}}Data = {{nameModel.pascalCase()}}Model.dummy();
  const String _data{{nameModel.pascalCase()}}Path = '{{nameStringPath}}';
  group('Enter Your Name Group',(){
    test('Enter Your Test Process',() async{
      when(
  () => mockJarvis.{{#isQuery}}query{{/isQuery}}{{^isQuery}}mutation{{/isQuery}}<{{nameModel.pascalCase()}}Model>(
      queryDocument: queryDocument.{{queryDocument.camelCase()}}(),
      param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy(),
      dataPath: _data{{nameModel.pascalCase()}}Path),
      ).thenAnswer((_) async => _t{{nameModel.pascalCase()}}Data);
      final result = await {{name.camelCase()}}RemoteDataSourceImpl.{{methodRepo.camelCase()}}(param:{{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
      expect(result,_t{{nameModel.pascalCase()}}Data);
      verify(
  () => mockJarvis.{{#isQuery}}query{{/isQuery}}{{^isQuery}}mutation{{/isQuery}}<{{nameModel.pascalCase()}}Model>(
      queryDocument: queryDocument.{{queryDocument.camelCase()}}(),
      param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy(),
      dataPath: _data{{nameModel.pascalCase()}}Path),).called(1);
      verifyNoMoreInteractions(mockJarvis);
    });
    test('should throw excption from remote data source',()async{
      when(
    () => mockJarvis.{{#isQuery}}query{{/isQuery}}{{^isQuery}}mutation{{/isQuery}}<{{nameModel.pascalCase()}}Model>(
         queryDocument: queryDocument.{{queryDocument.camelCase()}}(),
        param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy(),
        dataPath: _data{{nameModel.pascalCase()}}Path),
  ).thenThrow(UnknownException(message: 'message'));
      final call = {{name.camelCase()}}RemoteDataSourceImpl.{{methodRepo.camelCase()}}(param:{{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
      expect(() => call, throwsA(const TypeMatcher<UnknownException>()));
    });
    });
}