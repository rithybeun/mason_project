import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

void main(){
  late Mock{{nameRepository.pascalCase()}}Repository mock{{nameRepository.pascalCase()}}Repository;
  late {{nameUseCase.pascalCase()}}UseCase {{nameUseCase.camelCase()}}UseCase;
  setUp((){
    mock{{nameRepository.pascalCase()}}Repository = Mock{{nameRepository.pascalCase()}}Repository();
    {{nameUseCase.camelCase()}}UseCase = {{nameUseCase.pascalCase()}}UseCase();
  });
  final _t{{nameModel}}Entity = {{nameModel}}Model.dummy();
  group('right side >',(){
    test('should return the right side of {{nameModel}}Entity from {{nameRepository}}Repository',() async {
      when(() => mock{{nameRepository.pascalCase()}}Repository.{{nameMethodRepository.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).thenAnswer((_) async => Right(_t{{nameModel.pascalCase()}}Entity));
      final result = await {{nameUseCase.camelCase()}}UseCase(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
      expect(result,Right(_t{{nameModel}}Entity));
      verify(() => mock{{nameRepository.pascalCase()}}Repository.{{nameMethodRepository.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).called(1);
      verifyNoMoreInteractions(mock{{nameRepository.pascalCase()}}Repository);
    });
  });
  group('left side >',(){
    test('should return the lef side of failure [Failure]',(){
      when(() => mock{{nameRepository.pascalCase()}}Repository.{{nameMethodRepository.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).thenAnswer((_) async => Left(Failure.dummy()));
    final result = await {{nameUseCase.camelCase()}}UseCase(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy());
      expect(result,Left(Failure.dummy()));
      verify(() => mock{{nameRepository.pascalCase()}}Repository.{{nameMethodRepository.camelCase()}}(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())).called(1);
      verifyNoMoreInteractions(mock{{nameRepository.pascalCase()}}Repository);
    });
  });

}