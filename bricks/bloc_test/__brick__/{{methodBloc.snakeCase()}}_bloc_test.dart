import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class Mock{{methodUseCase.pascalCase()}}UseCase extends Mock implements {{methodUseCase.pascalCase()}}UseCase{}

void main(){
  late Mock{{methodUseCase.pascalCase()}}UseCase mock{{methodUseCase.pascalCase()}}UseCase;
  late {{methodBloc.pascalCase()}}Bloc {{methodBloc.camelCase()}}Bloc;
  setUp((){
    mock{{methodUseCase.pascalCase()}}UseCase = Mock{{methodUseCase.pascalCase()}}UseCase();
    {{methodBloc.camelCase()}}Bloc = {{methodBloc.pascalCase()}}Bloc({{methodUseCase.camelCase()}}UseCase: mock{{methodUseCase.pascalCase()}}UseCase);
  });
  final _t{{nameModel.pascalCase()}}Entity = {{nameModel.pascalCase()}}Model.dummy();
  const String msgFailure = 'unknow message';

  group("Enter your group process", () {
  blocTest<{{methodBloc.pascalCase()}}Bloc, {{methodBloc.pascalCase()}}State>(
      'emits state [{{stateBloc.pascalCase()}}Loaded()] when {{stateBloc.pascalCase()}}Event is called',
      build: () {
        when(() =>
                mock{{methodUseCase.pascalCase()}}UseCase(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy()))
            .thenAnswer((_) async => Right(_t{{nameModel.pascalCase()}}Entity));
        return {{methodBloc.camelCase()}}Bloc;
      },
      act: (bloc) =>
          bloc.add({{stateBloc.pascalCase()}}Event(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())),
      expect: () => [
            {{#isLoading}}
            {{methodBloc.pascalCase()}}Initial(
              authState: {{stateBloc.pascalCase()}}Loading(),),
            {{/isLoading}}
            {{^isLoading}}
            {{/isLoading}}
            {{methodBloc.pascalCase()}}Initial(
              authState: {{stateBloc.pascalCase()}}Loaded(),
              {{argumentBloc.camelCase()}}: _t{{nameModel.pascalCase()}}Entity,
            ),
          ],
      verify: (_) {
        verify(
          () => mock{{methodUseCase.pascalCase()}}UseCase(param:  {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy()),
        ).called(1);
        verifyNoMoreInteractions(mock{{methodUseCase.pascalCase()}}UseCase);
      });
  blocTest<{{methodBloc.pascalCase()}}Bloc, {{methodBloc.pascalCase()}}State>(
      'emits state [{{stateBloc.pascalCase()}}Failure()] when {{stateBloc.pascalCase()}}Event is called',
      build: () {
        when(() =>
                mock{{methodUseCase.pascalCase()}}UseCase(param:{{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy()))
            .thenAnswer(
                (_) async => const Left(UnknownFailure(message: msgFailure)));
        return {{methodBloc.camelCase()}}Bloc;
      },
      act: (bloc) =>
          bloc.add({{stateBloc.pascalCase()}}Event(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy())),
      expect: () => [
            {{#isLoading}}
            {{methodBloc.pascalCase()}}Initial(
              authState: {{stateBloc.pascalCase()}}Loading(),),
            {{/isLoading}}
            {{^isLoading}}
            {{/isLoading}}
            {{methodBloc.pascalCase()}}Initial(
              authState: {{stateBloc.pascalCase()}}Failure(),
              failure: const UnknownFailure(message: msgFailure),
            ),
          ],
      verify: (_) {
        verify(
          () => mock{{methodUseCase.pascalCase()}}UseCase(param: {{#isQuery}}Query{{/isQuery}}{{^isQuery}}Mutation{{/isQuery}}{{paramName.pascalCase()}}Param.dummy()),
        ).called(1);
        verifyNoMoreInteractions(mock{{methodUseCase.pascalCase()}}UseCase);
      });
});

}